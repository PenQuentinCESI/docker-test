CREATE TABLE cesi.utilisateur
(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nom VARCHAR(100),
	prenom VARCHAR(100),
	email VARCHAR(255),
	date_naissance DATE,
	pays VARCHAR(255),
	ville VARCHAR(255),
	code_postal VARCHAR(10)
);

INSERT INTO cesi.utilisateur
VALUES
	(1, 'EL BARAKA', 'Mehdy', 'mehdy@docker.com', '1995-09-24', 'France', 'Brest', '29200');