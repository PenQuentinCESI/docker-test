<html lang="fr">
<head>
    <title>Test docker</title>
    <meta charset='utf8'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <h1>Formulaire test</h1>
<form action="save.php" method="post">
    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" name="nom">
    </div>
    <div class="form-group">
        <label for="prenom">Prénom</label>
        <input type="text" class="form-control" id="prenom" name="prenom">
    </div>
    <div class="form-group">
        <label for="mail">Mail</label>
        <input type="email" class="form-control" id="mail" name="mail">
    </div>
    <div class="form-group">
        <label for="dateNaissance">Date de naissance</label>
        <input type="date" class="form-control" id="dateNaissance" name="dateNaissance">
    </div>
    <div class="form-group">
        <label for="pays">Pays</label>
        <input type="text" class="form-control" id="pays" name="pays">
    </div>
    <div class="form-group">
        <label for="ville">Ville</label>
        <input type="text" class="form-control" id="ville" name="ville">
    </div>
    <div class="form-group">
        <label for="codePostal">Code postal</label>
        <input type="text" class="form-control" id="codePostal" name="codePostal">
    </div>
    <button type="submit" class="btn btn-primary">Envoyer</button>
</form>
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Nom</th>
            <th scope="col">Prénom</th>
            <th scope="col">Mail</th>
            <th scope="col">Date de naissance</th>
            <th scope="col">Pays</th>
            <th scope="col">Ville</th>
            <th scope="col">Code postal</th>
        </tr>
        </thead>
        <tbody>
        <?php
        try {
            $dbh = new PDO('mysql:host=mysql;dbname=cesi', 'root', 'root');
            foreach ($dbh->query('SELECT * from utilisateur') as $u) {
            echo("
        <tr>
            <td>$u[nom]</td>
            <td>$u[prenom]</td>
            <td>$u[email]</td>
            <td>$u[date_naissance]</td>
            <td>$u[pays]</td>
            <td>$u[ville]</td>
            <td>$u[code_postal]</td>
        </tr>
                ");
            }
            $dbh = null;
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
        ?>
        </tbody>
    </table>
</div>
</body>
</html>
