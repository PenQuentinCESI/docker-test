# Création d'un environnement de développement web avec Docker

## INSTALLATION

Récupérer les sources du projet sur git:

SSH ``git clone git@gitlab.com:PenQuentinCESI/docker-test.git`` dans une invite de commande

**ou**

HTTPS ``https://gitlab.com/PenQuentinCESI/docker-test.git`` dans une invite de commande

**ou**

 En téléchargeant les sources depuis https://gitlab.com/PenQuentinCESI/docker-test
 
 ## CRÉATION DES FICHIERS DE CONFIGURATIOIN
 
* Se placer dans le dossier des sources ``cd docker-test`` dans une invite de commande

### Créer un fichier **docker-compose.yml**

Contenu du fichier :

```yaml
---

version: '3'

services:
    web-nginx:
        image: nginx:stable-alpine
        container_name: web-nginx
        volumes:
            - "./nginx/www:/usr/share/nginx/html:ro"
            - "./nginx/log:/var/log/nginx"
            - "./nginx/nginx.conf:/etc/nginx/nginx.conf:ro"
        ports:
            - "8987:80"
        depends_on:
            - web-php
            - mysqldb

    web-php:
        build: .
        container_name: web-php
        volumes:
            - "./nginx/www:/script:ro"

    myadmin:
        image: phpmyadmin/phpmyadmin
        container_name: phpmyadmin
        ports:
            - "8900:80"
        environment:
            - PMA_ARBITRARY=1
            - PMA_HOST=${MYSQL_HOST}
        restart: always
        depends_on:
            - mysqldb

    mysqldb:
        image: mysql:${MYSQL_VERSION}
        container_name: ${MYSQL_HOST}
        restart: always
        env_file:
            - ".env"
        environment:
            - MYSQL_DATABASE=${MYSQL_DATABASE}
            - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
            - MYSQL_USER=${MYSQL_USER}
            - MYSQL_PASSWORD=${MYSQL_PASSWORD}
        ports:
            - "8989:3306"
        volumes:
            - "./data/db/mysql:/var/lib/mysql"
```

### Créer un fichier **Dockerfile**
  
 
 ```bash
FROM php:fpm-alpine
 RUN docker-php-ext-install pdo pdo_mysql mysqli
```

## GÉRER L'APPLICATION

Vérifier la syntaxe de **docker-compose.yml** : ``docker-compose config``

Démarrer l'application : ``docker-compose up -d``

Voir le status des containers : ``docker-compose ps``

Ouvrir votre navigateur : 

* http://localhost:8987 (Application)

* http://localhost:8900 (PHP MyAdmin | host: mysql, username: root, password: root)

Importer **db_test.sql** dans la base de données **cesi** via l'interface php MyAdmin

Arrêter l'application : ``docker-compose stop``

Arrêter et détruire les services : ``docker-compose down``
